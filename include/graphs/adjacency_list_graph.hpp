#ifndef ADJACENCY_LIST_GRAPH_HPP_
#define ADJACENCY_LIST_GRAPH_HPP_

#include <memory>
#include <vector>

#include "graphs/graph.hpp"

class AdjacencyListGraph : public Graph
{
    /**
     * @brief Structure representing adjacency list
     * @param endVertexIndex Index of the ending vertex.
     * @param cost Cost of the connection between two vertices.
     */
    struct adjacency_list
    {
        int endVertexIndex;
        int cost;
    };

    /**
     * @brief List of vertices and their ending vertices with cost
     */
    std::vector<std::vector<adjacency_list>> listGraph_;

    /**
     * @brief Container with all edges in the graph
     */
    std::vector<std::vector<int>> edges_;

    /**
     * @brief Number of vertices in graph
     */
    int noVertices_;

  public:
    /**
     * @brief Construct a new Adjacency List Graph object
     * @param size number of vertices
     *
     * Construct a new Adjacency List Graph object with the given parameters. Resize parent vector
     * and assign number of vertices.
     */
    explicit AdjacencyListGraph(const int& size) : listGraph_(size), noVertices_(size){};

    /**
     * @brief Construct a new Adjacency List Graph object
     * @param otherListGraph another Adjacency List Graph class object
     *
     * Construct a new Adjacency List Graph object as a copy of the given Adjacency List.
     */
    AdjacencyListGraph(const AdjacencyListGraph& otherListGraph);

    /**
     * @brief Get all edges of the graph
     * @return std::vector<std::vector<int>> - Container with all edges containing
     * source index, destination index and cost between this indexes
     */
    [[nodiscard]] std::vector<std::vector<int>> edges() const override;

    /**
     * @brief Method to get cost between two vertices
     * @param beginVertex Index of the beginning vertex
     * @param endVertex Index of the ending vertex
     * @return int - Cost between two vertices
     */
    int operator()(const int& beginVertex, const int& endVertex) override;

    /**
     * @brief Display the Adjacency List Graph
     *
     * Display a Adjacency List as a table with vertices in rows.
     */
    void print() const override;

    /**
     * @brief Return number of vertices in the Adjacency List
     *
     * @return int noVertices - number of vertices
     */
    [[nodiscard]] int vertices() const override;

    /**
     * @brief Create a Graph object
     * @param is input stream with file containing Adjacency List Graph
     * @return std::unique_ptr<Graph> - Pointer to base class.
     *
     * Create a new Adjacency List from file given as a parameter.
     */
    static std::unique_ptr<Graph> createGraph(std::istream& is);

    /**
     * @brief Destroy the Adjacency List Graph object
     *
     * Destroy the Adjacency List Graph object and free allocated memory.
     */
    ~AdjacencyListGraph() override;
};

#endif /* ADJACENCY_LIST_GRAPH_HPP_ */
