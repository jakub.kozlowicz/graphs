#ifndef GRAPH_HPP_
#define GRAPH_HPP_

#include <vector>

class Graph
{
  public:
    /**
     * @brief Get all edges of the graph
     * @return std::vector<std::vector<int>> - Container with all edges containing
     * source index, destination index and cost between this indexes
     */
    [[nodiscard]] virtual std::vector<std::vector<int>> edges() const = 0;

    /**
     * @brief Virtual method to get cost between two vertices
     * @param beginVertex Index of the beginning vertex
     * @param endVertex Index of the ending vertex
     * @return int - Cost between two vertices
     */
    virtual int operator()(const int& beginVertex, const int& endVertex) = 0;

    /**
     * @brief Virtual method to display the graph
     */
    virtual void print() const = 0;

    /**
     * @brief Return number of vertices in the graph
     * @return int - number of vertices
     */
    [[nodiscard]] virtual int vertices() const = 0;

    /**
     * @brief Destroy the Graph object
     *
     * Virtual destructor of the Graph object.
     */
    virtual ~Graph() = default;
};

#endif /* GRAPH_HPP_ */
