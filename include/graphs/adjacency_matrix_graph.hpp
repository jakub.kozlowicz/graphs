#ifndef ADJACENCY_MATRIX_GRAPH_HPP_
#define ADJACENCY_MATRIX_GRAPH_HPP_

#include <memory>
#include <vector>

#include "graphs/graph.hpp"

class AdjacencyMatrixGraph : public Graph
{
    /**
     * @brief Two dimensional array of vertices and edges
     */
    int** matrixGraph_;

    /**
     * @brief Container with all edges in the graph
     */
    std::vector<std::vector<int>> edges_;

    /**
     * @brief Number of vertices in the graph
     */
    int noVertices_;

  public:
    /**
     * @brief Construct a new Adjacency Matrix Graph object
     * @param size size of the 2D Adjacency Matrix
     *
     * Construct a new Adjacency Matrix Graph object with the given parameters. Allocate
     * memory for two dimensional array. Assign to each item max value of int.
     * Also assing number of vertices.
     */
    explicit AdjacencyMatrixGraph(const int& size);

    /**
     * @brief Construct a new Adjacency Matrix Graph object
     * @param copy another Adjacency Matrix Graph class object
     *
     * Construct a new Adjacency Matrix Graph object as a copy of the given Adjacency Matrix.
     */
    AdjacencyMatrixGraph(const AdjacencyMatrixGraph& copy);

    /**
     * @brief Get all edges of the graph
     * @return std::vector<std::vector<int>> - Container with all edges containing
     * source index, destination index and cost between this indexes
     */
    [[nodiscard]] std::vector<std::vector<int>> edges() const override;

    /**
     * @brief Method to get cost between two vertices
     * @param beginVertex Index of the beginning vertex
     * @param endVertex Index of the ending vertex
     * @return int - Cost between two vertices
     */
    int operator()(const int& beginVertex, const int& endVertex) override;

    /**
     * @brief Display the Adjacency Matrix Graph
     *
     * Display a Adjacency Matrix as a two dimensional array.
     */
    void print() const override;

    /**
     * @brief Return number of vertices in the Adjacency Matrix
     * @return int noVertices - number of vertices
     */
    [[nodiscard]] int vertices() const override;

    /**
     * @brief Create a Graph object
     * @param is input stream with file containing Adjacency Matrix Graph
     * @return std::unique_ptr<Graph> - Pointer to base class.
     *
     * Create a new Adjacency Matrix from file given as a parameter.
     */
    static std::unique_ptr<Graph> createGraph(std::istream& is);

    /**
     * @brief Destroy the Adjacency Matrix Graph object
     *
     * Destroy the Adjacency Matrix Graph object and free allocated memory.
     */
    ~AdjacencyMatrixGraph() override;
};

#endif /* ADJACENCY_MATRIX_GRAPH_HPP_ */
