#include "graphs/adjacency_matrix_graph.hpp"

#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>

AdjacencyMatrixGraph::AdjacencyMatrixGraph(const int& size)
{
    noVertices_ = size;

    matrixGraph_ = new int*[size];
    for(int row = 0; row < size; ++row) matrixGraph_[row] = new int[size];

    for(int row = 0; row < size; ++row)
    {
        for(int column = 0; column < size; ++column)
        {
            matrixGraph_[row][column] = std::numeric_limits<int>::max();
        }
    }
}

AdjacencyMatrixGraph::AdjacencyMatrixGraph(const AdjacencyMatrixGraph& otherMatrixGraph)
    : AdjacencyMatrixGraph(otherMatrixGraph.noVertices_)
{
    for(size_t row = 0; row < noVertices_; ++row)
    {
        for(size_t column = 0; column < noVertices_; ++column)
        {
            matrixGraph_[row][column] = otherMatrixGraph.matrixGraph_[row][column];
        }
    }

    edges_ = otherMatrixGraph.edges_;
}

std::vector<std::vector<int>> AdjacencyMatrixGraph::edges() const
{
    return edges_;
}

int AdjacencyMatrixGraph::operator()(const int& beginVertex, const int& endVertex)
{
    return matrixGraph_[beginVertex][endVertex];
}

void AdjacencyMatrixGraph::print() const
{
    for(size_t row = 0; row < noVertices_; ++row)
    {
        for(size_t column = 0; column < noVertices_; ++column)
        {
            std::cout << matrixGraph_[row][column];
        }
        std::cout << "\n";
    }
}

int AdjacencyMatrixGraph::vertices() const
{
    return noVertices_;
}

std::unique_ptr<Graph> AdjacencyMatrixGraph::createGraph(std::istream& is)
{
    std::string line;
    std::getline(is, line);
    std::istringstream iss{line};

    int size, connections;
    iss >> size >> connections;

    AdjacencyMatrixGraph resultGraph(size);

    while(connections-- > 0)
    {
        std::getline(is, line);
        std::istringstream issg{line};

        int beginVertex, endVertex, cost;
        issg >> beginVertex >> endVertex >> cost;

        std::vector<int> edge{beginVertex, endVertex, cost};
        resultGraph.edges_.push_back(edge);

        resultGraph.matrixGraph_[beginVertex][endVertex] = cost;
    }

    return std::make_unique<AdjacencyMatrixGraph>(resultGraph);
}

AdjacencyMatrixGraph::~AdjacencyMatrixGraph()
{
    for(int row = 0; row < noVertices_; ++row)
    {
        delete[] matrixGraph_[row];
    }

    delete[] matrixGraph_;
}