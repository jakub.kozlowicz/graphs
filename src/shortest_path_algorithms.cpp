#include "graphs/shortest_path_algorithms.hpp"

#include <algorithm>
#include <limits>

const int INFINITY = std::numeric_limits<int>::max();

int getMin(ShortestPathResult& result, const std::vector<bool>& visited, const int& noVertices)
{
    int key = 0, min = INFINITY;

    for(int i = 0; i < noVertices; ++i)
    {
        if(!visited[i] && result[i].cost < min)
        {
            min = result[i].cost;
            key = i;
        }
    }

    return key;
}

void path(ShortestPathResult& result, const int parent[], const int& noVertices)
{
    for(int i = 0; i < noVertices; ++i)
    {
        int tmp = parent[i];
        while(tmp != -1)
        {
            result[i].path.push_back(tmp);
            tmp = parent[tmp];
        }

        std::reverse(result[i].path.begin(), result[i].path.end());
        result[i].path.push_back(i);
    }
}

void dijkstra(Graph& graph, int sourceIndex, ShortestPathResult& result)
{
    std::vector<bool> visited(graph.vertices(), false);
    int parent[graph.vertices()];

    for(int i = 0; i < graph.vertices(); ++i)
    {
        result[i].cost = INFINITY;
    }

    result[sourceIndex].cost = 0;
    parent[sourceIndex] = -1;

    for(int i = 0; i < graph.vertices() - 1; ++i)
    {
        int foundIndex = getMin(result, visited, graph.vertices());
        visited[foundIndex] = true;

        for(int currentIndex = 0; currentIndex < graph.vertices(); ++currentIndex)
        {
            if(!visited[currentIndex] &&
               (result[foundIndex].cost + graph(foundIndex, currentIndex)) < result[currentIndex].cost &&
               graph(foundIndex, currentIndex) != INFINITY)
            {
                parent[currentIndex] = foundIndex;
                result[currentIndex].cost = result[foundIndex].cost + graph(foundIndex, currentIndex);
            }
        }
    }

    path(result, parent, graph.vertices());
}

bool bellmanFord(Graph& graph, int sourceIndex, ShortestPathResult& result)
{
    int parent[graph.vertices()];

    for(int i = 0; i < graph.vertices(); ++i)
    {
        result[i].cost = INFINITY;
    }

    result[sourceIndex].cost = 0;
    parent[sourceIndex] = -1;

    auto edges = graph.edges();
    auto vertices = graph.vertices();

    while(--vertices)
    {
        for(auto& edge : edges)
        {
            int source = edge[0], destination = edge[1], cost = edge[2];

            if(result[source].cost != INFINITY && result[source].cost + cost < result[destination].cost)
            {
                result[destination].cost = result[source].cost + cost;
                parent[destination] = source;
            }
        }
    }

    path(result, parent, graph.vertices());

    for(auto& edge : edges)
    {
        int source = edge[0], destination = edge[1], cost = edge[2];

        if(result[source].cost != INFINITY && result[source].cost + cost < result[destination].cost)
        {
            return false;
        }
    }

    return true;
}
