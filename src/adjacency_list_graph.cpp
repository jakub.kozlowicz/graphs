#include "graphs/adjacency_list_graph.hpp"

#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>

AdjacencyListGraph::AdjacencyListGraph(const AdjacencyListGraph& otherListGraph)
    : AdjacencyListGraph(otherListGraph.listGraph_.size())
{
    for(size_t i = 0; i < listGraph_.size(); ++i)
    {
        listGraph_[i] = otherListGraph.listGraph_[i];
    }

    edges_ = otherListGraph.edges_;
}

std::vector<std::vector<int>> AdjacencyListGraph::edges() const
{
    return edges_;
}

int AdjacencyListGraph::operator()(const int& beginVertex, const int& endVertex)
{
    for(const auto& adjacency : listGraph_[beginVertex])
    {
        if(adjacency.endVertexIndex == endVertex) return adjacency.cost;
    }

    return std::numeric_limits<int>::max();
}

void AdjacencyListGraph::print() const
{
    for(size_t i = 0; i < listGraph_.size(); ++i)
    {
        std::cout << i << ": ";

        for(const auto& item : listGraph_[i])
        {
            std::cout << "(" << item.endVertexIndex << ", " << item.cost << ") ";
        }
        std::cout << "\n";
    }
}

int AdjacencyListGraph::vertices() const
{
    return noVertices_;
}

std::unique_ptr<Graph> AdjacencyListGraph::createGraph(std::istream& is)
{

    std::string line;
    std::getline(is, line);
    std::istringstream iss{line};

    int size, connections;
    iss >> size >> connections;

    AdjacencyListGraph resultGraph(size);

    while(connections-- > 0)
    {
        std::getline(is, line);
        std::istringstream issg{line};

        int beginVertex, endVertex, cost;
        issg >> beginVertex >> endVertex >> cost;

        std::vector<int> edge{beginVertex, endVertex, cost};
        resultGraph.edges_.push_back(edge);

        adjacency_list end{endVertex, cost};
        resultGraph.listGraph_[beginVertex].push_back(end);
    }

    return std::make_unique<AdjacencyListGraph>(resultGraph);
}

AdjacencyListGraph::~AdjacencyListGraph() = default;