#include "graphs/adjacency_list_graph.hpp"
#include "graphs/adjacency_matrix_graph.hpp"
#include "graphs/shortest_path_algorithms.hpp"
#include "timer.hpp"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <thread>

using namespace std::string_literals;

const std::filesystem::path dataDirectoryPath{DATA_DIR_PATH};
const std::filesystem::path resultDirectoryPath{RESULT_DIR_PATH};

const int REPEAT = 50;

std::vector<std::string> readFiles(std::ifstream& stream)
{
    std::vector<std::string> files;

    while(stream)
    {
        if(stream.eof())
        {
            break;
        }
        std::string line;
        stream >> line;
        files.push_back(line);
    }

    return files;
}

double measureAlgorytm(void (*algorytm)(Graph&, int, ShortestPathResult&), Graph& graph, int sourceIndex,
                       ShortestPathResult& result)
{
    Timer timer;

    timer.start();
    (*algorytm)(graph, sourceIndex, result);
    timer.stop();

    return timer.msInterval();
}

double measureAlgorytm(bool (*algorytm)(Graph&, int, ShortestPathResult&), Graph& graph, int sourceIndex,
                       ShortestPathResult& result)
{
    Timer timer;

    timer.start();
    (*algorytm)(graph, sourceIndex, result);
    timer.stop();

    return timer.msInterval();
}

void testAlgorytm(const std::vector<std::string>& inputGraphs, const std::string& file, const bool& algorytm,
                  const bool& dataType)
{
    std::ofstream resultFile;
    resultFile.open(resultDirectoryPath / file);

    if(resultFile.good())
    {
        for(const auto& inputGraph : inputGraphs)
        {
            std::ifstream inputGraphFile;

            if(algorytm)
            {
                inputGraphFile.open(resultDirectoryPath / "negative" / inputGraph);
            }
            else
            {
                inputGraphFile.open(resultDirectoryPath / "positive" / inputGraph);
            }

            if(inputGraphFile.good())
            {
                auto graphsFiles = readFiles(inputGraphFile);
                for(const auto& file : graphsFiles)
                {
                    double time = 0.0;

                    for(size_t i = 0; i < REPEAT; i++)
                    {
                        std::ifstream inputFileGraph;
                        inputFileGraph.open(dataDirectoryPath / "graph" / file);

                        if(inputFileGraph.good())
                        {
                            std::unique_ptr<Graph> graph;
                            if(dataType)
                            {
                                graph = AdjacencyListGraph::createGraph(inputFileGraph);
                            }
                            else
                            {
                                graph = AdjacencyMatrixGraph::createGraph(inputFileGraph);
                            }

                            int sourceIndex;
                            inputFileGraph >> sourceIndex;

                            ShortestPathResult result;

                            if(algorytm)
                            {
                                time += measureAlgorytm(bellmanFord, *graph, sourceIndex, result);
                            }
                            else
                            {
                                time += measureAlgorytm(dijkstra, *graph, sourceIndex, result);
                            }
                        }
                        inputFileGraph.close();
                    }

                    double average = time / REPEAT;
                    resultFile << file << ", " << average << "\n";
                }

                resultFile << "\n";
                inputGraphFile.close();
            }
        }
    }

    resultFile.close();
}

int main(int argc, char* argv[])
{
    std::vector<std::string> inputGraphs{"graphs10.txt", "graphs20.txt",  "graphs30.txt",  "graphs50.txt",
                                         "graphs70.txt", "graphs100.txt", "graphs150.txt", "graphs200.txt"};

    std::map<std::string, std::string> type{{"bellman_list", "bellman_ford_list.csv"},
                                            {"bellman_matrix", "bellman_ford_matrix.csv"},
                                            {"dijkstra_list", "dijkstra_list.csv"},
                                            {"dijkstra_matrix", "dijkstra_matrix.csv"}};

    bool bellman = true, dijkstra = false, list = true, matrix = false;

    std::thread t1 = std::thread(testAlgorytm, std::ref(inputGraphs), std::string(type["bellman_list"]),
                                 std::ref(bellman), std::ref(list));
    std::thread t2 = std::thread(testAlgorytm, std::ref(inputGraphs), std::string(type["bellman_matrix"]),
                                 std::ref(bellman), std::ref(matrix));
    std::thread t3 = std::thread(testAlgorytm, std::ref(inputGraphs), std::string(type["dijkstra_list"]),
                                 std::ref(dijkstra), std::ref(list));
    std::thread t4 = std::thread(testAlgorytm, std::ref(inputGraphs), std::string(type["dijkstra_matrix"]),
                                 std::ref(dijkstra), std::ref(matrix));

    t1.join();
    t2.join();
    t3.join();
    t4.join();

    return 0;
}
